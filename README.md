# events-list-front
# Sinopse
Front da [web api de eventos](https://gitlab.com/DanielFrag/events-list)
# Decisões de projeto
- Desenvolvido em React
- Desacoplado da web api de eventos
# Dependências
- Node >=10.x.x
- npm >=6.x.x
# Instalar pacotes
No diretório do projeto execute o comando
```
$ npm i
```
# Rodar o projeto
- Em modo de desenvolvimento
```
$ npm start
```
# Build
Os arquivos a serem servidos ficaram disponíveis no diretório "build" após a execução do comando
```
$ npm run build
```
