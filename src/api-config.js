const getApiUrl = () => {
  if (process.env.NODE_ENV === 'development' || process.env.NODE_ENV === 'test') {
    return 'http://localhost:8080/api'
  }
  return 'http://127.0.0.1:8080/api'
}
const API_URL = getApiUrl();

export default API_URL;