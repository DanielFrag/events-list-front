import API_URL from '../api-config'

const getEventsList = async (prefix) => {
  try {

    const urlSearchParams = new URLSearchParams();
    if (prefix && typeof prefix === 'string') {
      urlSearchParams.append('prefix', prefix);
    }
    const qs = urlSearchParams.toString();
    const res = await fetch(`${API_URL}/events${qs ? `?${qs}` : ''}`);
    if (!res.status) {
      return {
        error: 'Unknow error',
        details: res.error()
      }
    }
    if (res.status === 204) {
      return [];
    }
    if (res.status === 200) {
      return res.json();
    }
    return {
      error: `Status http error: ${res.status}`,
      details: res.text()
    }
  } catch(e) {
    return {
      error: 'Unknow error',
      details: e.message
    };
  }
}

export default { getEventsList };
