import React from 'react';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';

const Home = ({
  content
}) => {
  return (
    <div>
      <Grid
        container
        justify="center"
      >
        <Grid
          item
          xs={9}
        >
          <div
            style={{
              height: '20vh',
              display: 'inline-block',
              textAlign: 'center',
              padding: '24px 0px',
              color: '#313131'
            }}
          >
            <h1>
              Events List
            </h1>
          </div>
          <Paper>
            { content }
          </Paper>
        </Grid>
      </Grid>
    </div>
  );
}

export default Home
