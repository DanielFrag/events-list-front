import React, { useState } from 'react';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import eventsRepo from '../../../repository/events-repository';

const InputAutoComplete = ({
  onEvent
}) => {
  const [performingQuery, setPerformingQuery] = useState(false);
  const [options, setOptions] = useState([])
  const checkInputLength = async (value) => {
    if (value.length < 2 || performingQuery) {
      return;
    }
    setPerformingQuery(true);
    const result = await eventsRepo.getEventsList(value);
    setPerformingQuery(false);
    if (result.error) {
      return onEvent('error', result);
    }
    setOptions(result);
  };
  return (
    <Autocomplete
      options={options.sort((a, b) => a.localeCompare(b))}
      getOptionLabel={option => option}
      renderInput={params => (
        <TextField
          {...params}
          label="Busca de Eventos"
          variant="outlined"
          fullWidth
          onChange={e => checkInputLength(e.target.value)}
        />
      )}
    />
  );
}
export default InputAutoComplete;
