import React, { useState } from 'react';
import InputAutoComplete from './component/organisms/InputAutoComplete/InputAutoComplete';
import Home from './component/pages/Home/Home';
import Snackbar from '@material-ui/core/Snackbar';
import './App.css';

function App() {
  const [errorFlag, setErrorFlag] = useState(false);
  const [errorData, setErrorData] = useState('');
  const listener = (event, data) => {
    if (event === 'error') {
      setErrorData(data.details);
      return setErrorFlag(true);
    }
    console.log('listener called');
  }
  return (
    <div className="App">
      <Home
        content={<InputAutoComplete
          onEvent={listener}
        />}
      />
      <Snackbar
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'center'
        }}
        open={errorFlag}
        onClose={() => setErrorFlag(false)}
        autoHideDuration={3000}
        message={errorData}
      />
    </div>
  );
}

export default App;
